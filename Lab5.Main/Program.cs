﻿using System;
using System.Windows;

using PK.Container;
using Container;
using Lab5.Infrastructure;

using Lab5.DisplayForm;
using Lab5.MainComponent.Specification;


namespace Lab5.Main
{
    public class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            ApplicationStart();

            // Należy zaimplementować tę metodę
            IContainer container = Configuration.ConfigureApp();

            /*** Początek własnego kodu ***/
            ICore core = container.Resolve<ICore>();
           // ICore core = container.Resolve(typeof(ICore)) as ICore;
            core.WlaczPrad();
            Console.ReadKey();

            /*** Koniec własnego kodu ***/

            ApplicationStop();
        }

        private static void DisplayFormExample()
        {
            var model = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
            {
                var form = new Form();
                var viewModel = new DisplayViewModel();
                form.DataContext = viewModel;
                form.Show();
                return viewModel;
            }), null);

            model.Text = "Test";
        }

        public static void ApplicationStart()
        {
            var thread = new System.Threading.Thread(CreateApp);
            thread.SetApartmentState(System.Threading.ApartmentState.STA);
            thread.Start();
            System.Threading.Thread.Sleep(300);
        }

        private static void CreateApp()
        {
            var app = new Application();
            app.ShutdownMode = ShutdownMode.OnExplicitShutdown;
            app.Run();
        }

        public static void ApplicationStop()
        {
            Application.Current.Dispatcher.InvokeShutdown();
        }

    }
}
