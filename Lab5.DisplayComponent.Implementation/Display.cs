﻿using Lab5.DisplayComponent.Specification;
using Lab5.DisplayForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xaml;

namespace Lab5.DisplayComponent.Implementation
{
    public class Display : IDisplay
    {
        private INotifyPropertyChanged notify;

        public Display(INotifyPropertyChanged notify)
        {
            this.notify = notify;
        }

        public void ShowText(string text)
        {
            var model = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
            {
                var form = new Form();
                var viewModel = new DisplayViewModel();
                form.DataContext = viewModel;
                form.Show();
                return viewModel;
            }), null);

            model.Text = text;

        }

    }
}
