﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.DisplayComponent.Specification
{
    public interface IDisplay
    {
        void ShowText(string text);
    }
}
