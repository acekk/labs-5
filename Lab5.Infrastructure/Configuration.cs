﻿using System;
using PK.Container;
using Lab5.DisplayComponent.Implementation;
using Lab5.DisplayForm;
using Lab5.MainComponent.Implementation;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            IContainer container = new Container.Implementation.Container();

            container.Register(typeof(Core));
            container.Register(typeof(Display));
            container.Register(typeof(DisplayViewModel));

            return container;
        }
    }
}
