﻿using Lab5.DisplayComponent.Implementation;
using Lab5.DisplayComponent.Specification;
using Lab5.MainComponent.Implementation;
using Lab5.MainComponent.Specification;
using Container;
using PK.Container;
using System;
using System.Reflection;


namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Container.Implementation.Container);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(ICore));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Core));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(IDisplay));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Display));

        #endregion
    }
}
