﻿using Lab5.DisplayComponent.Specification;
using Lab5.MainComponent.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Lab5.MainComponent.Implementation
{
    public class Core : ICore
    {
        private IDisplay display;

        public Core(IDisplay display)
        {
            this.display = display;
        }

        public void WlaczPrad()
        {
            display.ShowText("Wlaczono");
        }
    }
}
